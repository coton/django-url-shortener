import { useEffect } from "react";
import { useRouter } from "next/router";
import { ShortenerURLResponse } from "../types";

export default function RedirectPage() {
  const router = useRouter();
  const { shortId } = router.query;

  /**
   * On Load, get the
   */
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(
          `${process.env.NEXT_PUBLIC_API_URL}/api/${shortId}`
        );
        if (response.status == 404) {
          router.push(`${window.location.origin}/404`);
        }

        const data: ShortenerURLResponse = await response.json();
        router.push(data.url);
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, [shortId]);

  return;
}
