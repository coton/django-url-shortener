import { useState } from "react";
import { ShortenerURLResponse } from "../types";

export default function IndexPage() {
  const [url, setUrl] = useState<string>("");
  const [res, setResponse] = useState<ShortenerURLResponse | null>(null);

  const handleOnClick = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/`, {
        method: "POST",
        body: JSON.stringify({ url }),
        headers: {
          "content-type": "application/json",
        },
      });
      setResponse((await response.json()) as ShortenerURLResponse);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="h-screen w-screen bg-gray-200 flex flex-col place-items-center justify-center">
      <div className="bg-white lg:w-1/2 rounded-xl shadow-lg flex flex-col gap-4 p-4">
        <h1 className="place-items-center w-auto font-bold text-2xl">
          URL Shortener
        </h1>
        {res ? (
          <p>
            Shortened URL:{" "}
            <span data-cy="short-url">{`${window.location.origin}/${res.short_id}`}</span>
          </p>
        ) : (
          <>
            <p>Enter a URL to shorten</p>
            <input
              type="text"
              name="url"
              data-cy="url-input"
              placeholder="Set Url here..."
              className="border-gray-400 border m-1 p-2 rounded-lg"
              onChange={(event) => setUrl(event.target.value)}
            />
            <button
              className="bg-purple-400 w-fit py-2 px-4 rounded-xl border-purple-800 border self-center"
              data-cy="url-submit"
              onClick={handleOnClick}
            >
              Submit
            </button>
          </>
        )}
      </div>
    </div>
  );
}
