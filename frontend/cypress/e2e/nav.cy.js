describe("Link Creation", () => {
  it("should create a shortened URL", () => {
    cy.visit("/");
    cy.get("[data-cy=url-input]").type("https://google.fr");
    cy.get("[data-cy=url-submit]").click();
    cy.get("[data-cy=short-url]").should("be.visible");
  });

  it("should redirect if the code is OK", async () => {
    cy.get("[data-cy=short-url]").then(($span) => {
      cy.visit($span.text());
      cy.url().should("include", "https://google.fr");
    });
  });

  it("should go to /404 if code is wrong", () => {
    cy.visit("/WrOnGCode____");
    cy.url().should("include", "/404");
  });
});
