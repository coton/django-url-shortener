export type ShortenerURLResponse = {
  url: string;
  short_id: string;
};
