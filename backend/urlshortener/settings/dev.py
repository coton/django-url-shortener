from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': getenv("POSTGRES_DB", None),
        'USER': getenv("POSTGRES_USER", None),
        'PASSWORD': getenv("POSTGRES_PASSWORD", None),
        'HOST': getenv("POSTGRES_HOSTNAME", "db"),
        'PORT': getenv("POSTGRES_PORT", 5432),
    }
}
