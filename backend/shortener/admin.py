from django.contrib import admin

from shortener.models import ShortenedURL


# Register your models here.
admin.site.register(ShortenedURL)