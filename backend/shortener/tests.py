import json

from rest_framework.test import APITestCase
from rest_framework import status
from django.test import TestCase

from shortener.models import ShortenedURL
from unittest.mock import patch


def mock_generate_id():
    return "ABCDEF123456"


class ShortenedUrlTestCase(TestCase):
    @patch("shortener.models.generate_id", mock_generate_id)
    def test_can_create_a_shortened_url_by_model(self):
        s = ShortenedURL.objects.create(url="https://www.google.fr")
        self.assertTrue(s.url == "https://www.google.fr")
        self.assertTrue(s.short_id == "ABCDEF123456")


class ShortenedUrlAPITestCase(APITestCase):
    @patch("shortener.models.generate_id", mock_generate_id)
    def test_can_create_a_shortened_url_by_model(self):

        res = self.client.post('/api/', json.dumps({'url': "https://www.duckduckgo.com" }), content_type="application/json")
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
 
        s = ShortenedURL.objects.get(short_id="ABCDEF123456")
        self.assertTrue(s.url == "https://www.duckduckgo.com")
        self.assertTrue(s.short_id == "ABCDEF123456")

    @patch("shortener.models.generate_id", mock_generate_id)
    def test_can_access_a_shortened_url(self):
        ShortenedURL.objects.create(url="https://www.google.fr")
        res = self.client.get('/api/ABCDEF123456/')
        data = res.json()

        self.assertEqual(data["url"], "https://www.google.fr")
        self.assertEqual(data["short_id"], "ABCDEF123456")

    def test_list_is_forbidden(self):
        ShortenedURL.objects.create(url="https://www.google.fr")
        res = self.client.get('/api/')
        self.assertEqual(res.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_update_is_forbidden(self):
        s = ShortenedURL.objects.create(url="https://www.google.fr")
        res = self.client.put(f'/api/{s.short_id}/', { "url": "https://www.duckduckgo.com"})
        self.assertEqual(res.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_delete_is_forbidden(self):
        s = ShortenedURL.objects.create(url="https://www.google.fr")
        res = self.client.delete(f'/api/{s.short_id}/')
        self.assertEqual(res.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)