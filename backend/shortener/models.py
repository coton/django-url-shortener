import random
import string
from uuid import uuid4

from django.db import models


ID_LENGTH = 16
def generate_id():
    return ''.join(random.choice(string.ascii_letters) for i in range(ID_LENGTH))


class BaseModel(models.Model):
    id = models.UUIDField(default=uuid4, primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class ShortenedURL(BaseModel):
    short_id = models.CharField(default=None, max_length=ID_LENGTH, unique=True)
    url = models.TextField()

    def save(self, *args, **kwargs):
        while not self.short_id or ShortenedURL.objects.filter(short_id=self.short_id).count():
            self.short_id = generate_id()
        super(BaseModel, self).save(*args, **kwargs)


    def __str__(self):
        return f"<ShortenedURL - {self.short_id} - {self.url:.15}...>"


    class Meta:
        indexes = [
            models.Index(fields=['short_id'])
        ]