from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin

from shortener.serializers import ShortenedURLSerializer
from shortener.models import ShortenedURL


class ShortenedURLView(RetrieveModelMixin, CreateModelMixin, GenericViewSet):
    """
    API Endpoint that generates or retrieve shortened URLs.

    retrieve: 
    Retrieve an URL via its short ID
    
    create:
    Creates a shortened URL
    """
    queryset = ShortenedURL.objects.all()
    serializer_class = ShortenedURLSerializer
    lookup_field = 'short_id'
