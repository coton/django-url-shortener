from rest_framework import routers
from rest_framework.schemas import get_schema_view

from django.views.generic import TemplateView
from django.urls import path

from shortener.views import ShortenedURLView

router = routers.SimpleRouter()
router.register(r'', ShortenedURLView)

schema_view = get_schema_view(
    title = "42 - URL Shortener",
    description = "URL Shortener API",
    version = "0.1.0"
)

urlpatterns = [
    path("openapi", schema_view, name='openapi-schema'),
    path("docs/", TemplateView.as_view(
        template_name="swagger.html",
        extra_context={'schema_url': "openapi-schema"}
    ), name="swagger-ui")
] + router.urls