from rest_framework.serializers import ModelSerializer

from shortener.models import ShortenedURL

class ShortenedURLSerializer(ModelSerializer):
    class Meta:
        model = ShortenedURL
        fields = ("url", "short_id",)
        read_only_fields = ("short_id",)
        