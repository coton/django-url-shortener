# 42 Url Shortener

## Quick Start

```
cp .env.example .env
docker-compose up -d
```

Endpoint list:

- [http://localhost:3000/]() : Frontend
- [http://localhost:8080/admin]() : Admin Backend
- [http://localhost:8080/api/docs]() : API Documentation

## Run tests

### Backend

1. Run the stack with `docker-compose up -d`
2. Execute `docker-compose exec backend python3 manage.py test`

### Frontend

NB: Frontend Tests will be tested as end2end tests outside the stack. It needs a node environment on your workstation + yarn installed

```
Install yarn:
- sudo npm install -g yarn
```

1. Run the stack with `docker-compose up -d`
2. Go to `/frontend`
3. Run `yarn cypress install`
4. Run `yarn cypress run`

## Access Admin panel

Before accessing the admin panel, you need to create a superuser:

```
docker-compose exec backend python3 manage.py createsuperuser
```

Fill up the form with name, email and password.

Then, go to [http://localhost:8080/admin]() and log in.

You can access to all Shortened URLs from this panel.

## Quick notes and further ameliorations

This repository contains a separate frontend and backend codebase.

### Backend

For the backend, I've used Django + Django Rest Framework to create a simple REST API that handles both URL shortening + shortened url retrieving. Datas are stocked in a PostgreSQL database.

_What can I do for improving this?_

- Add a URL checker to verify if the sended URL is correctly formated
- Add IP-Based rate limiting, to prevent flood
- Add a time-based shortened URL that deletes automatically when it reach a certain period of time
- Add destination URL verification (like, prevent a user that its link goes to nowhere or it 404 for some reasons...)
- Add accounts and authentication + account based features like owned shortenedURLs list or stats
- Switch to a GraphQL endpoint

### Frontend

For the frontend, I've used React + Next.js + TailwindCSS.

What pushes me to choose those techs is to have more features available to me on client side, compared to a 100% server-side app.

- React because the ecosystem is really dense, full of powerful components and with a more programmating style than things like Vue or Svelte. You create .tsx files with TypeScript and JSX, and you create components, you're not using templates.
- Tailwind for all the CSS-part, which is a simple yet powerful tool to stylish your component, based on a bunch of little CSS classes
- Next.js for the Routing/ServerSide Handling part (which can combine very well with Vercel).

_What can I do for improving this?_

- Better design (because... 4 hours you know...)
- Front side URL parsing
- User UI with plans
- Deployment on Vercel
- SEO
